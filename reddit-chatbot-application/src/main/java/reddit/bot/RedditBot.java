package reddit.bot;

import reddit.response.IMessageDispatcher;
import reddit.response.RedditMessageDispatcher;
import reddit.response.RedditMessageResponseListener;
import reddit.web.RedditClient;
import chatbot.client.ClientConnection;
import chatbot.client.ConnectionListener;
import chatbot.client.MessageChannel;
import chatbot.client.MessageListener;
import chatbot.response.ResponseStrategy;

public class RedditBot implements ConnectionListener {

	private final ClientConnection clientConnection;
	private final String channelName;
	private final ResponseStrategy responseStrategy;

	public RedditBot(ClientConnection connection, String channelName, ResponseStrategy responseStrategy) {
		this.clientConnection = connection;
		this.channelName = channelName;
		this.responseStrategy = responseStrategy;
	}

	public void connect() {
		this.clientConnection.connect(this);
	}

	@Override
	public void onConnectionSuccessful() {
		MessageChannel channel = this.clientConnection.channel(this.channelName);
		IMessageDispatcher dispatcher = new RedditMessageDispatcher(channel);
		RedditClient redditClient = new RedditClient(dispatcher);
		MessageListener messageListener = new RedditMessageResponseListener(this.responseStrategy, redditClient);
		channel.registerListener(messageListener);
	}

}
