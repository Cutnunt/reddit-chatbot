package reddit.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import reddit.pojo.Subreddit;
import reddit.unmarshall.SubredditJsonUnmarshaller;

public class RedditJSonProcessor implements IResponseProcessor {
	
	private static final Logger logger = LoggerFactory.getLogger(RedditJSonProcessor.class);
	
	private IMessageDispatcher dispatcher;
	
	public RedditJSonProcessor(IMessageDispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}

	public void processMessage(String jsonResponse) {
		Subreddit subreddit = SubredditJsonUnmarshaller.unmarshalJson(jsonResponse);
		logger.info("Json response successfully unmarshalled: {}", jsonResponse);
		dispatcher.dispatchMessage(subreddit.toString());
	}

}
