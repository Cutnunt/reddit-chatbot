package reddit.response;

import chatbot.client.Message;
import chatbot.response.ResponseStrategy;

public class CommandParameterResponseStrategy implements ResponseStrategy {

	private static final String SPACE_REGEX = " ";
	
	private final String botUsername;

	public CommandParameterResponseStrategy(String botUsername) {
		this.botUsername = botUsername;
	}
	
	public boolean shouldRespond(Message message) {
		return isCommand(message.getPayload());
	}

	private boolean isCommand(String payload) {
		String[] split = payload.split(SPACE_REGEX);
		String command = split[0];
		if (command == "") {
			return false;
		}
		return split[0].equals(botUsername) && split.length == 2;
	}

}
