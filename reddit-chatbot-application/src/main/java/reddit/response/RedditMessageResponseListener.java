package reddit.response;

import reddit.web.RedditClient;
import chatbot.client.Message;
import chatbot.client.MessageListener;
import chatbot.response.ResponseStrategy;

public class RedditMessageResponseListener implements MessageListener {

	private final ResponseStrategy responseStrategy;
	private final RedditClient redditClient;
	
	public RedditMessageResponseListener(ResponseStrategy responseStrategy, RedditClient redditClient) {
		this.responseStrategy = responseStrategy;
		this.redditClient = redditClient;
	}
	
	public void onMessageReceived(Message message) {
		if (this.responseStrategy.shouldRespond(message)) {
			this.redditClient.processMessage(message);
		}
	}
}
