package reddit.response;

import chatbot.client.MessageChannel;

public class RedditMessageDispatcher implements IMessageDispatcher {
	
	private final MessageChannel channel;
	
	public RedditMessageDispatcher(MessageChannel channel) {
		this.channel = channel;
	}

	@Override
	public synchronized void dispatchMessage(String message) {
		System.out.println("Dispatching message");
		this.channel.sendMessage(message);
	}

}
