package reddit.response;

public interface IMessageDispatcher {

	public void dispatchMessage(String message);
}
