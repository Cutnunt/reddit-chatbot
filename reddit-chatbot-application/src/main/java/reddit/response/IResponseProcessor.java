package reddit.response;

public interface IResponseProcessor {
	
	public void processMessage(String message);
}
