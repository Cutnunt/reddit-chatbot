package reddit.http;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

public class HttpAsynchronousRunnable implements Runnable {
	
	private static final int HTTP_OK = 200;
	
	private final HttpClient httpClient;
	private final HttpMethod method;
	private final HttpResponseListener responseListener;
	
	public HttpAsynchronousRunnable(HttpMethod method, HttpResponseListener responseListener) {
		this.method = method;
		this.responseListener = responseListener;
		this.httpClient = new HttpClient();
	}

	public void run() {
		Integer responseCode = null;
		try {
			responseCode = httpClient.executeMethod(method);
		} catch (Exception e) {
			throw new RuntimeException("An error occured when making Http call", e);
		}
		System.out.println("Http response code [" + responseCode + "]");
		notifyListener(responseCode);
	}
	
	private void notifyListener(Integer responseCode) {
		if (responseCode == HTTP_OK) {
			this.responseListener.onResponseReceived();
		}
	}
	
}
