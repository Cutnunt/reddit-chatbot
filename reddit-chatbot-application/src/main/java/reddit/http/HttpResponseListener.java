package reddit.http;

public interface HttpResponseListener {

	public void onResponseReceived();
	
}
