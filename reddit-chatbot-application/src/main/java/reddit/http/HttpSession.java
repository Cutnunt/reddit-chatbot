package reddit.http;

import java.io.IOException;

import org.apache.commons.httpclient.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import reddit.response.IResponseProcessor;

public class HttpSession implements HttpResponseListener {
	
	private static Logger logger = LoggerFactory.getLogger(HttpSession.class);

	private final HttpMethod method;
	
	private final IResponseProcessor responseProcessor;
	
	public HttpSession(HttpMethod method, IResponseProcessor responseProcessor) {
		this.method = method;
		this.responseProcessor = responseProcessor;
	}

	public void onResponseReceived() {
		String responseBodyAsString = null; 
		try {
			responseBodyAsString = method.getResponseBodyAsString();
		} catch (IOException e) {
			logger.info("Error occurred getting http response from {}", method.getName());
		}
		responseProcessor.processMessage(responseBodyAsString);
	}

}
