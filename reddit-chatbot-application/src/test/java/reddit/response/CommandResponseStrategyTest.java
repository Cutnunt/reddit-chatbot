package reddit.response;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import chatbot.client.Message;

public class CommandResponseStrategyTest {

	private static final String BOT_NAME = "Reddit";
	
	private CommandParameterResponseStrategy commandResponseStrategy;
	
	@Before
	public void setup() {
		this.commandResponseStrategy = new CommandParameterResponseStrategy(BOT_NAME);
	}
	
	@Test
	public void testOnlyBotNameMessageWithSubredditParameter() {
		Message message = new Message("User", BOT_NAME + " politics");
		assertThat(commandResponseStrategy.shouldRespond(message), is(true));
	}
	
	@Test
	public void testGivenEmptyMessage() {
		Message message = new Message("User", "");
		assertThat(commandResponseStrategy.shouldRespond(message), is(false));
	}
	
	@Test
	public void testParameterStringContent() {
		// TODO find out allowed letters in a subreddit name and apply that check
	}
}
