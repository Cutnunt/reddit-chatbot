package reddit.chatbot.subredditrequest;

import pircbot.adapter.PircClientConnection;
import reddit.bot.RedditBot;
import reddit.response.CommandParameterResponseStrategy;
import chatbot.client.ClientConnection;

public class RedditIrc {
	
	private static final String NICKNAME = "Reddit";

	private final RedditBot chatBot;

	public static void main(String[] args) {
		ClientConnection clientConnection = new PircClientConnection("irc.uk.quakenet.org", 6667, NICKNAME, "lolomg");
		RedditIrc ircBot = new RedditIrc(clientConnection, "#bhynjk");
		ircBot.start();
	}
	
	public void start() {
		this.chatBot.connect();
	}

	public RedditIrc(ClientConnection connection, String channel) {
		CommandParameterResponseStrategy responseStrategy = new CommandParameterResponseStrategy(NICKNAME);
		this.chatBot = new RedditBot(connection, channel, responseStrategy);
	}
}
