package pircbot.adapter;

import org.jibble.pircbot.NickAlreadyInUseException;

import chatbot.client.ClientConnection;
import chatbot.client.ConnectionListener;
import chatbot.client.MessageChannel;

public class PircClientConnection implements ClientConnection {
	
	private final PircChatEngine chatEngine;
	private final String password;

	public PircClientConnection(String server, int port, String username,String password) {
		this.password = password;
		this.chatEngine = new PircChatEngine(server, port, username);
	}

	@Override
	public void connect(ConnectionListener listener) {
		registerConnectionListener(listener);
		try {
			this.chatEngine.connect();
		} catch (NickAlreadyInUseException e) {
			throw new RuntimeException("Nick already in use");
		} catch (Exception e) {
			throw new RuntimeException("Something bad happend connecting to server");
		}
	}

	@Override
	public MessageChannel channel(String name) {
		MessageChannel messageChannel = chatEngine.connectChannel(name, password);
		return messageChannel;
	}
	
	private void registerConnectionListener(ConnectionListener listener) {
		this.chatEngine.addChatEngineListener(listener);
	}
}