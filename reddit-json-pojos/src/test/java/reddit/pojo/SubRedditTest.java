package reddit.pojo;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import reddit.pojo.SubredditListing;
import reddit.pojo.Subreddit;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SubRedditTest {
	
	private static final String SUBREDDITJSON = "target/test-classes/proggit.json";
	
	@Test
	public void testDeserializes() throws JsonParseException, JsonMappingException, IOException {
		File jsonFile = new File(SUBREDDITJSON);
		assertThat(jsonFile.isFile(), equalTo(true));
		
		ObjectMapper objectMapper = new ObjectMapper();
		Subreddit subReddit = objectMapper.readValue(jsonFile, Subreddit.class);
		assertThat(subReddit, notNullValue());
		assertThat(subReddit.getData(), notNullValue());
		assertThat(subReddit.getKind(), equalTo("Listing"));
		
		List<SubredditListing> redditPosts = subReddit.getData().getChildren();
		assertThat(redditPosts.size(), is(25));
	}

}
