package reddit.pojo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;

import org.junit.Test;

import reddit.pojo.SubredditListing;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PostTest {

	@Test
	public void testDeserializes() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		SubredditListing post = objectMapper.readValue(JSONPOST, SubredditListing.class);
		
		assertThat(post, notNullValue());
		assertThat(post.getKind(), equalTo("t3"));
		assertThat(post.getData(), notNullValue());
		assertThat(post.getData().getUrl(), equalTo("http://www.yuiblog.com/blog/2012/03/30/what-is-the-meaning-of-this"));
	}
	
	private static final String JSONPOST = "{\n" +
            "            \"kind\" : \"t3\",\n" +
            "            \"data\" : {\n" +
            "                \"domain\" : \"yuiblog.com\",\n" +
            "                \"banned_by\" : null,\n" +
            "                \"media_embed\" : {},\n" +
            "                \"subreddit\" : \"programming\",\n" +
            "                \"selftext_html\" : null,\n" +
            "                \"selftext\" : \"\",\n" +
            "                \"likes\" : null,\n" +
            "                \"saved\" : false,\n" +
            "                \"id\" : \"rmjwk\",\n" +
            "                \"clicked\" : false,\n" +
            "                \"title\" : \"Douglas Crockford on Fat Arrow Functions in JavaScript\",\n" +
            "                \"num_comments\" : 24,\n" +
            "                \"score\" : 64,\n" +
            "                \"approved_by\" : null,\n" +
            "                \"over_18\" : false,\n" +
            "                \"hidden\" : false,\n" +
            "                \"thumbnail\" : \"\",\n" +
            "                \"subreddit_id\" : \"t5_2fwo\",\n" +
            "                \"author_flair_css_class\" : null,\n" +
            "                \"downs\" : 25,\n" +
            "                \"is_self\" : false,\n" +
            "                \"permalink\" : \"/r/programming/comments/rmjwk/douglas_crockford_on_fat_arrow_functions_in/\",\n" +
            "                \"name\" : \"t3_rmjwk\",\n" +
            "                \"created\" : 1333239308,\n" +
            "                \"url\" : \"http://www.yuiblog.com/blog/2012/03/30/what-is-the-meaning-of-this\",\n" +
            "                \"author_flair_text\" : null,\n" +
            "                \"author\" : \"swizec\",\n" +
            "                \"created_utc\" : 1333214108,\n" +
            "                \"media\" : null,\n" +
            "                \"num_reports\" : null,\n" +
            "                \"ups\" : 89\n" +
            "            }\n" +
            "        }";
}
