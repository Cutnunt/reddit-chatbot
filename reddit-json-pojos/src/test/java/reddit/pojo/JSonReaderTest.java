package reddit.pojo;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSonReaderTest {

	private static final File jsonFile = new File(
			"target/test-classes/proggit.json");

	@Test
	public void test() throws JsonProcessingException, IOException {
		Assert.assertTrue(jsonFile.isFile());

		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode node = objectMapper.readTree(jsonFile);
		Assert.assertNotNull(node);
	}
}
