package reddit.pojo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import reddit.pojo.SubredditListing;
import reddit.pojo.SubredditData;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SubRedditDataTest {

	@Test
	public void testDeserializes() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		SubredditData subRedditData = objectMapper.readValue(SUBREDDITDATA, SubredditData.class);
		
		assertThat(subRedditData, notNullValue());
		
		List<SubredditListing> redditPosts = subRedditData.getChildren();
		assertThat(redditPosts.size(), equalTo(3));
	}
	
	private final static String SUBREDDITDATA = "{\n" +
            "        \"modhash\" : \"lg3hlfsgw7d39526dc8550d7e2c12f63af88d1100fd588c41f\",\n" +
            "        \"children\" : [{\n" +
            "            \"kind\" : \"t3\",\n" +
            "            \"data\" : {\n" +
            "                \"subreddit\" : \"programming\",\n" +
            "                \"title\" : \"Douglas Crockford on Fat Arrow Functions in JavaScript\",\n" +
            "                \"num_comments\" : 24,\n" +
            "                \"score\" : 64,\n" +
            "                \"downs\" : 25,\n" +
            "                \"permalink\" : \"/r/programming/comments/rmjwk/douglas_crockford_on_fat_arrow_functions_in/\",\n" +
            "                \"url\" : \"http://www.yuiblog.com/blog/2012/03/30/what-is-the-meaning-of-this\",\n" +
            "                \"ups\" : 89\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"kind\" : \"t3\",\n" +
            "            \"data\" : {\n" +
            "                \"subreddit\" : \"programming\",\n" +
            "                \"title\" : \"The next step after forking is spooning..\",\n" +
            "                \"num_comments\" : 3,\n" +
            "                \"score\" : 25,\n" +
            "                \"downs\" : 4,\n" +
            "                \"permalink\" : \"/r/programming/comments/rmmef/the_next_step_after_forking_is_spooning/\",\n" +
            "                \"url\" : \"https://bitbucket.org/spooning/\",\n" +
            "                \"ups\" : 29\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"kind\" : \"t3\",\n" +
            "            \"data\" : {\n" +
            "                \"subreddit\" : \"programming\",\n" +
            "                \"title\" : \"Dead Programmers Aren't Much Fun\",\n" +
            "                \"num_comments\" : 26,\n" +
            "                \"score\" : 33,\n" +
            "                \"downs\" : 34,\n" +
            "                \"permalink\" : \"/r/programming/comments/rmhww/dead_programmers_arent_much_fun/\",\n" +
            "                \"url\" : \"http://www.whattofix.com/blog/archives/2012/03/dead-programmer.php\",\n" +
            "                \"ups\" : 67\n" +
            "            }\n" +
            "        }\n" +
            "        ],\n" +
            "        \"after\" : \"t3_rj22l\",\n" +
            "        \"before\" : null\n" +
            "    }";
}
