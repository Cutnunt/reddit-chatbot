package reddit.pojo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;

import org.junit.Test;

import reddit.pojo.SubredditListingData;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PostDataTest {

	@Test
	public void testDeserializes() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		SubredditListingData post = objectMapper.readValue(POSTJSON, SubredditListingData.class);
		assertThat(post, notNullValue());
		assertThat(post.getTitle(), equalTo("Douglas Crockford on Fat Arrow Functions in JavaScript"));
		assertThat(post.getScore(), equalTo(64));
		assertThat(post.getNumComments(), equalTo(24));
		assertThat(post.getUrl(), equalTo("http://www.yuiblog.com/blog/2012/03/30/what-is-the-meaning-of-this"));
	}
	
	private static final String POSTJSON = "{" +
            "\"domain\" : \"yuiblog.com\"," +
            "\"banned_by\" : null," +
            "\"media_embed\" : {}," +
            "\"subreddit\" : \"programming\"," +
            "\"selftext_html\" : null," +
            "\"selftext\" : \"\"," +
            "\"likes\" : null," +
            "\"saved\" : false," +
            "\"id\" : \"rmjwk\"," +
            "\"clicked\" : false," +
            "\"title\" : \"Douglas Crockford on Fat Arrow Functions in JavaScript\"," +
            "\"num_comments\" : 24," +
            "\"score\" : 64," +
            "\"approved_by\" : null," +
            "\"over_18\" : false," +
            "\"hidden\" : false," +
            "\"thumbnail\" : \"\"," +
            "\"subreddit_id\" : \"t5_2fwo\"," +
            "\"author_flair_css_class\" : null," +
            "\"downs\" : 25," +
            "\"is_self\" : false," +
            "\"permalink\" : \"/r/programming/comments/rmjwk/douglas_crockford_on_fat_arrow_functions_in/\"," +
            "\"name\" : \"t3_rmjwk\"," +
            "\"created\" : 1333239308," +
            "\"url\" : \"http://www.yuiblog.com/blog/2012/03/30/what-is-the-meaning-of-this\"," +
            "\"author_flair_text\" : null," +
            "\"author\" : \"swizec\"," +
            "\"created_utc\" : 1333214108," +
            "\"media\" : null," +
            "\"num_reports\" : null," +
            "\"ups\" : 89" +
        "}";
}

